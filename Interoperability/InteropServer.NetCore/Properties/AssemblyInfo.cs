﻿using System.Reflection;
using Tecan.Sila2;

[assembly: AssemblyTitle( "Tecan.Sila2.InteropServer.NetCore" )]
[assembly: AssemblyDescription( "An interoperability server implementation based on ASP.NET Core" )]

[assembly: VendorUri( "https://gitlab.com/SiLA2/vendors/sila_tecan" )]