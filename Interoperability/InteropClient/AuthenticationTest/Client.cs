//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Linq;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Server;

namespace Tecan.Sila2.Interop.Client.AuthenticationTest.AuthenticationTest
{
    
    
    /// <summary>
    /// Class that implements the IAuthenticationTest interface through SiLA2
    /// </summary>
    public partial class AuthenticationTestClient : IAuthenticationTest
    {
        
        private IClientExecutionManager _executionManager;
        
        private IClientChannel _channel;
        
        private const string _serviceName = "sila2.org.silastandard.test.authenticationtest.v1.AuthenticationTest";
        
        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="channel">The channel through which calls should be executed</param>
        /// <param name="executionManager">A component to determine metadata to attach to any requests</param>
        public AuthenticationTestClient(IClientChannel channel, IClientExecutionManager executionManager)
        {
            _executionManager = executionManager;
            _channel = channel;
        }
        
        /// <summary>
        /// Requires an authorization token in order to be executed
        /// </summary>
        public virtual void RequiresToken()
        {
            RequiresTokenRequestDto request = new RequiresTokenRequestDto(null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/AuthenticationTest/v1/Command/RequiresToken");
            try
            {
                _channel.ExecuteUnobservableCommand<RequiresTokenRequestDto>(_serviceName, "RequiresToken", request, callInfo);
                callInfo.FinishSuccessful();
                return;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        private T Extract<T>(Tecan.Sila2.ISilaTransferObject<T> dto)
        
        {
            return dto.Extract(_executionManager.DownloadBinaryStore);
        }
    }
    
    /// <summary>
    /// Factory to instantiate clients for the Authentication Test.
    /// </summary>
    [System.ComponentModel.Composition.ExportAttribute(typeof(IClientFactory))]
    [System.ComponentModel.Composition.PartCreationPolicyAttribute(System.ComponentModel.Composition.CreationPolicy.Shared)]
    public partial class AuthenticationTestClientFactory : IClientFactory
    {
        
        /// <summary>
        /// Gets the fully-qualified identifier of the feature for which clients can be generated
        /// </summary>
        public string FeatureIdentifier
        {
            get
            {
                return "org.silastandard/test/AuthenticationTest/v1";
            }
        }
        
        /// <summary>
        /// Gets the interface type for which clients can be generated
        /// </summary>
        public System.Type InterfaceType
        {
            get
            {
                return typeof(IAuthenticationTest);
            }
        }
        
        /// <summary>
        /// Creates a strongly typed client for the given execution channel and execution manager
        /// </summary>
        /// <param name="channel">The channel that should be used for communication with the server</param>
        /// <param name="executionManager">The execution manager to manage metadata</param>
        /// <returns>A strongly typed client. This object will be an instance of the InterfaceType property</returns>
        public object CreateClient(IClientChannel channel, IClientExecutionManager executionManager)
        {
            return new AuthenticationTestClient(channel, executionManager);
        }
    }
}

