//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Linq;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Server;

namespace Tecan.Sila2.Interop.Client.ErrorHandlingTest.ErrorHandlingTest
{
    
    
    /// <summary>
    /// Class that implements the IErrorHandlingTest interface through SiLA2
    /// </summary>
    public partial class ErrorHandlingTestClient : IErrorHandlingTest, System.ComponentModel.INotifyPropertyChanged, System.IDisposable
    {
        
        private System.Lazy<long> _raiseDefinedExecutionErrorOnGet;
        
        private long _raiseDefinedExecutionErrorOnSubscribe;
        
        private System.Threading.Tasks.Task _raiseDefinedExecutionErrorOnSubscribeUpdateTask;
        
        private System.Lazy<long> _raiseUndefinedExecutionErrorOnGet;
        
        private long _raiseUndefinedExecutionErrorOnSubscribe;
        
        private System.Threading.Tasks.Task _raiseUndefinedExecutionErrorOnSubscribeUpdateTask;
        
        private long _raiseDefinedExecutionErrorAfterValueWasSent;
        
        private System.Threading.Tasks.Task _raiseDefinedExecutionErrorAfterValueWasSentUpdateTask;
        
        private long _raiseUndefinedExecutionErrorAfterValueWasSent;
        
        private System.Threading.Tasks.Task _raiseUndefinedExecutionErrorAfterValueWasSentUpdateTask;
        
        private System.Threading.CancellationTokenSource _cancellationTokenSource = new System.Threading.CancellationTokenSource();
        
        private IClientExecutionManager _executionManager;
        
        private IClientChannel _channel;
        
        private const string _serviceName = "sila2.org.silastandard.test.errorhandlingtest.v1.ErrorHandlingTest";
        
        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="channel">The channel through which calls should be executed</param>
        /// <param name="executionManager">A component to determine metadata to attach to any requests</param>
        public ErrorHandlingTestClient(IClientChannel channel, IClientExecutionManager executionManager)
        {
            _executionManager = executionManager;
            _channel = channel;
            InitLazyRequests();
        }
        
        /// <summary>
        /// A property that raises a "Test Error" on get with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual long RaiseDefinedExecutionErrorOnGet
        {
            get
            {
                return _raiseDefinedExecutionErrorOnGet.Value;
            }
        }
        
        /// <summary>
        /// A property that raises a "Test Error" on subscribe with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual long RaiseDefinedExecutionErrorOnSubscribe
        {
            get
            {
                if ((_raiseDefinedExecutionErrorOnSubscribeUpdateTask == null))
                {
                    try
                    {
                        System.Threading.Monitor.Enter(_cancellationTokenSource);
                        if ((_raiseDefinedExecutionErrorOnSubscribeUpdateTask == null))
                        {
                            _raiseDefinedExecutionErrorOnSubscribeUpdateTask = _channel.SubscribeProperty<PropertyResponse<Tecan.Sila2.IntegerDto>>(_serviceName, "RaiseDefinedExecutionErrorOnSubscribe", "org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseDefinedExecutionErrorOnS" +
                                    "ubscribe", ReceiveNewRaiseDefinedExecutionErrorOnSubscribe, _executionManager.CreateCallOptions("org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseDefinedExecutionErrorOnS" +
                                        "ubscribe"), _cancellationTokenSource.Token);
                        }
                    } finally
                    {
                        System.Threading.Monitor.Exit(_cancellationTokenSource);
                    }
                }
                return _raiseDefinedExecutionErrorOnSubscribe;
            }
        }
        
        /// <summary>
        /// A property that raises an Undefined Execution Error on get with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual long RaiseUndefinedExecutionErrorOnGet
        {
            get
            {
                return _raiseUndefinedExecutionErrorOnGet.Value;
            }
        }
        
        /// <summary>
        /// A property that raises an Undefined Execution Error on subscribe with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual long RaiseUndefinedExecutionErrorOnSubscribe
        {
            get
            {
                if ((_raiseUndefinedExecutionErrorOnSubscribeUpdateTask == null))
                {
                    try
                    {
                        System.Threading.Monitor.Enter(_cancellationTokenSource);
                        if ((_raiseUndefinedExecutionErrorOnSubscribeUpdateTask == null))
                        {
                            _raiseUndefinedExecutionErrorOnSubscribeUpdateTask = _channel.SubscribeProperty<PropertyResponse<Tecan.Sila2.IntegerDto>>(_serviceName, "RaiseUndefinedExecutionErrorOnSubscribe", "org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseUndefinedExecutionErrorO" +
                                    "nSubscribe", ReceiveNewRaiseUndefinedExecutionErrorOnSubscribe, _executionManager.CreateCallOptions("org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseUndefinedExecutionErrorO" +
                                        "nSubscribe"), _cancellationTokenSource.Token);
                        }
                    } finally
                    {
                        System.Threading.Monitor.Exit(_cancellationTokenSource);
                    }
                }
                return _raiseUndefinedExecutionErrorOnSubscribe;
            }
        }
        
        /// <summary>
        /// A property that first sends the integer value 1 and then raises a Defined Execution Error with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual long RaiseDefinedExecutionErrorAfterValueWasSent
        {
            get
            {
                if ((_raiseDefinedExecutionErrorAfterValueWasSentUpdateTask == null))
                {
                    try
                    {
                        System.Threading.Monitor.Enter(_cancellationTokenSource);
                        if ((_raiseDefinedExecutionErrorAfterValueWasSentUpdateTask == null))
                        {
                            _raiseDefinedExecutionErrorAfterValueWasSentUpdateTask = _channel.SubscribeProperty<PropertyResponse<Tecan.Sila2.IntegerDto>>(_serviceName, "RaiseDefinedExecutionErrorAfterValueWasSent", "org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseDefinedExecutionErrorAft" +
                                    "erValueWasSent", ReceiveNewRaiseDefinedExecutionErrorAfterValueWasSent, _executionManager.CreateCallOptions("org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseDefinedExecutionErrorAft" +
                                        "erValueWasSent"), _cancellationTokenSource.Token);
                        }
                    } finally
                    {
                        System.Threading.Monitor.Exit(_cancellationTokenSource);
                    }
                }
                return _raiseDefinedExecutionErrorAfterValueWasSent;
            }
        }
        
        /// <summary>
        /// A property that first sends the integer value 1 and then raises a Undefined Execution Error with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual long RaiseUndefinedExecutionErrorAfterValueWasSent
        {
            get
            {
                if ((_raiseUndefinedExecutionErrorAfterValueWasSentUpdateTask == null))
                {
                    try
                    {
                        System.Threading.Monitor.Enter(_cancellationTokenSource);
                        if ((_raiseUndefinedExecutionErrorAfterValueWasSentUpdateTask == null))
                        {
                            _raiseUndefinedExecutionErrorAfterValueWasSentUpdateTask = _channel.SubscribeProperty<PropertyResponse<Tecan.Sila2.IntegerDto>>(_serviceName, "RaiseUndefinedExecutionErrorAfterValueWasSent", "org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseUndefinedExecutionErrorA" +
                                    "fterValueWasSent", ReceiveNewRaiseUndefinedExecutionErrorAfterValueWasSent, _executionManager.CreateCallOptions("org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseUndefinedExecutionErrorA" +
                                        "fterValueWasSent"), _cancellationTokenSource.Token);
                        }
                    } finally
                    {
                        System.Threading.Monitor.Exit(_cancellationTokenSource);
                    }
                }
                return _raiseUndefinedExecutionErrorAfterValueWasSent;
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        private long RequestRaiseDefinedExecutionErrorOnGet()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseDefinedExecutionErrorOnG" +
                    "et");
            try
            {
                long response = _channel.ReadProperty<PropertyResponse<Tecan.Sila2.IntegerDto>>(_serviceName, "RaiseDefinedExecutionErrorOnGet", "org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseDefinedExecutionErrorOnG" +
                        "et", callInfo).Value.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex, ConvertRaiseDefinedExecutionErrorOnGetException);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Converts the error ocurred during execution of RaiseDefinedExecutionErrorOnGet to a proper exception
        /// </summary>
        /// <param name="errorIdentifier">The identifier of the error that has happened</param>
        /// <param name="errorMessage">The original error message from the server</param>
        /// <returns>The converted exception or null, if the error is not understood</returns>
        private static System.Exception ConvertRaiseDefinedExecutionErrorOnGetException(string errorIdentifier, string errorMessage)
        {
            if ((errorIdentifier == "org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError"))
            {
                return new TestErrorException(errorMessage);
            }
            return null;
        }
        
        private void ReceiveNewRaiseDefinedExecutionErrorOnSubscribe(PropertyResponse<Tecan.Sila2.IntegerDto> raiseDefinedExecutionErrorOnSubscribe)
        {
            long inner = raiseDefinedExecutionErrorOnSubscribe.Value.Extract(_executionManager.DownloadBinaryStore);
            if ((_raiseDefinedExecutionErrorOnSubscribe != inner))
            {
                _raiseDefinedExecutionErrorOnSubscribe = inner;
                System.ComponentModel.PropertyChangedEventHandler handler = PropertyChanged;
                if ((handler != null))
                {
                    handler.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs("RaiseDefinedExecutionErrorOnSubscribe"));
                }
            }
        }
        
        private long RequestRaiseUndefinedExecutionErrorOnGet()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseUndefinedExecutionErrorO" +
                    "nGet");
            try
            {
                long response = _channel.ReadProperty<PropertyResponse<Tecan.Sila2.IntegerDto>>(_serviceName, "RaiseUndefinedExecutionErrorOnGet", "org.silastandard/test/ErrorHandlingTest/v1/Property/RaiseUndefinedExecutionErrorO" +
                        "nGet", callInfo).Value.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        private void ReceiveNewRaiseUndefinedExecutionErrorOnSubscribe(PropertyResponse<Tecan.Sila2.IntegerDto> raiseUndefinedExecutionErrorOnSubscribe)
        {
            long inner = raiseUndefinedExecutionErrorOnSubscribe.Value.Extract(_executionManager.DownloadBinaryStore);
            if ((_raiseUndefinedExecutionErrorOnSubscribe != inner))
            {
                _raiseUndefinedExecutionErrorOnSubscribe = inner;
                System.ComponentModel.PropertyChangedEventHandler handler = PropertyChanged;
                if ((handler != null))
                {
                    handler.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs("RaiseUndefinedExecutionErrorOnSubscribe"));
                }
            }
        }
        
        private void ReceiveNewRaiseDefinedExecutionErrorAfterValueWasSent(PropertyResponse<Tecan.Sila2.IntegerDto> raiseDefinedExecutionErrorAfterValueWasSent)
        {
            long inner = raiseDefinedExecutionErrorAfterValueWasSent.Value.Extract(_executionManager.DownloadBinaryStore);
            if ((_raiseDefinedExecutionErrorAfterValueWasSent != inner))
            {
                _raiseDefinedExecutionErrorAfterValueWasSent = inner;
                System.ComponentModel.PropertyChangedEventHandler handler = PropertyChanged;
                if ((handler != null))
                {
                    handler.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs("RaiseDefinedExecutionErrorAfterValueWasSent"));
                }
            }
        }
        
        private void ReceiveNewRaiseUndefinedExecutionErrorAfterValueWasSent(PropertyResponse<Tecan.Sila2.IntegerDto> raiseUndefinedExecutionErrorAfterValueWasSent)
        {
            long inner = raiseUndefinedExecutionErrorAfterValueWasSent.Value.Extract(_executionManager.DownloadBinaryStore);
            if ((_raiseUndefinedExecutionErrorAfterValueWasSent != inner))
            {
                _raiseUndefinedExecutionErrorAfterValueWasSent = inner;
                System.ComponentModel.PropertyChangedEventHandler handler = PropertyChanged;
                if ((handler != null))
                {
                    handler.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs("RaiseUndefinedExecutionErrorAfterValueWasSent"));
                }
            }
        }
        
        /// <summary>
        /// Disposes the current client and cancels all subscriptions
        /// </summary>
        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
        }
        
        /// <summary>
        /// Initializes lazies for non-observable properties.
        /// </summary>
        private void InitLazyRequests()
        {
            _raiseDefinedExecutionErrorOnGet = new System.Lazy<long>(RequestRaiseDefinedExecutionErrorOnGet);
            _raiseUndefinedExecutionErrorOnGet = new System.Lazy<long>(RequestRaiseUndefinedExecutionErrorOnGet);
        }
        
        /// <summary>
        /// Raises the "Test Error" with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual void RaiseDefinedExecutionError()
        {
            RaiseDefinedExecutionErrorRequestDto request = new RaiseDefinedExecutionErrorRequestDto(null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/ErrorHandlingTest/v1/Command/RaiseDefinedExecutionError");
            try
            {
                _channel.ExecuteUnobservableCommand<RaiseDefinedExecutionErrorRequestDto>(_serviceName, "RaiseDefinedExecutionError", request, callInfo);
                callInfo.FinishSuccessful();
                return;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex, ConvertRaiseDefinedExecutionErrorException);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Converts the error ocurred during execution of RaiseDefinedExecutionError to a proper exception
        /// </summary>
        /// <param name="errorIdentifier">The identifier of the error that has happened</param>
        /// <param name="errorMessage">The original error message from the server</param>
        /// <returns>The converted exception or null, if the error is not understood</returns>
        private static System.Exception ConvertRaiseDefinedExecutionErrorException(string errorIdentifier, string errorMessage)
        {
            if ((errorIdentifier == "org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError"))
            {
                return new TestErrorException(errorMessage);
            }
            return null;
        }
        
        /// <summary>
        /// Raises the "Test Error" with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual Tecan.Sila2.IObservableCommand RaiseDefinedExecutionErrorObservably()
        {
            RaiseDefinedExecutionErrorObservablyRequestDto request = new RaiseDefinedExecutionErrorObservablyRequestDto(null);
            request.Validate();
            return _channel.ExecuteObservableCommand<RaiseDefinedExecutionErrorObservablyRequestDto>(_serviceName, "RaiseDefinedExecutionErrorObservably", request, ConvertRaiseDefinedExecutionErrorObservablyException, _executionManager.CreateCallOptions(request.CommandIdentifier));
        }
        
        /// <summary>
        /// Converts the error ocurred during execution of RaiseDefinedExecutionErrorObservably to a proper exception
        /// </summary>
        /// <param name="errorIdentifier">The identifier of the error that has happened</param>
        /// <param name="errorMessage">The original error message from the server</param>
        /// <returns>The converted exception or null, if the error is not understood</returns>
        private static System.Exception ConvertRaiseDefinedExecutionErrorObservablyException(string errorIdentifier, string errorMessage)
        {
            if ((errorIdentifier == "org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError"))
            {
                return new TestErrorException(errorMessage);
            }
            return null;
        }
        
        /// <summary>
        /// Raises an Undefined Execution Error with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual void RaiseUndefinedExecutionError()
        {
            RaiseUndefinedExecutionErrorRequestDto request = new RaiseUndefinedExecutionErrorRequestDto(null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/ErrorHandlingTest/v1/Command/RaiseUndefinedExecutionError");
            try
            {
                _channel.ExecuteUnobservableCommand<RaiseUndefinedExecutionErrorRequestDto>(_serviceName, "RaiseUndefinedExecutionError", request, callInfo);
                callInfo.FinishSuccessful();
                return;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Raises an Undefined Execution Error with the error message 'SiLA2_test_error_message'
        /// </summary>
        public virtual Tecan.Sila2.IObservableCommand RaiseUndefinedExecutionErrorObservably()
        {
            RaiseUndefinedExecutionErrorObservablyRequestDto request = new RaiseUndefinedExecutionErrorObservablyRequestDto(null);
            request.Validate();
            return _channel.ExecuteObservableCommand<RaiseUndefinedExecutionErrorObservablyRequestDto>(_serviceName, "RaiseUndefinedExecutionErrorObservably", request, ConvertRaiseUndefinedExecutionErrorObservablyException, _executionManager.CreateCallOptions(request.CommandIdentifier));
        }
        
        /// <summary>
        /// Converts the error ocurred during execution of RaiseUndefinedExecutionErrorObservably to a proper exception
        /// </summary>
        /// <param name="errorIdentifier">The identifier of the error that has happened</param>
        /// <param name="errorMessage">The original error message from the server</param>
        /// <returns>The converted exception or null, if the error is not understood</returns>
        private static System.Exception ConvertRaiseUndefinedExecutionErrorObservablyException(string errorIdentifier, string errorMessage)
        {
            return null;
        }
        
        private T Extract<T>(Tecan.Sila2.ISilaTransferObject<T> dto)
        
        {
            return dto.Extract(_executionManager.DownloadBinaryStore);
        }
    }
    
    /// <summary>
    /// Factory to instantiate clients for the Error Handling Test.
    /// </summary>
    [System.ComponentModel.Composition.ExportAttribute(typeof(IClientFactory))]
    [System.ComponentModel.Composition.PartCreationPolicyAttribute(System.ComponentModel.Composition.CreationPolicy.Shared)]
    public partial class ErrorHandlingTestClientFactory : IClientFactory
    {
        
        /// <summary>
        /// Gets the fully-qualified identifier of the feature for which clients can be generated
        /// </summary>
        public string FeatureIdentifier
        {
            get
            {
                return "org.silastandard/test/ErrorHandlingTest/v1";
            }
        }
        
        /// <summary>
        /// Gets the interface type for which clients can be generated
        /// </summary>
        public System.Type InterfaceType
        {
            get
            {
                return typeof(IErrorHandlingTest);
            }
        }
        
        /// <summary>
        /// Creates a strongly typed client for the given execution channel and execution manager
        /// </summary>
        /// <param name="channel">The channel that should be used for communication with the server</param>
        /// <param name="executionManager">The execution manager to manage metadata</param>
        /// <returns>A strongly typed client. This object will be an instance of the InterfaceType property</returns>
        public object CreateClient(IClientChannel channel, IClientExecutionManager executionManager)
        {
            return new ErrorHandlingTestClient(channel, executionManager);
        }
    }
}

