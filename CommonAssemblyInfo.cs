﻿using System.Reflection;

[assembly: AssemblyCompany( "Tecan Trading AG" )]
[assembly: AssemblyCopyright( "Copyright 2019 by Tecan Trading AG, Switzerland" )]
[assembly: AssemblyProduct( "Tecan SiLA2 SDK" )]

// those entries will be overridden by the build
[assembly: AssemblyVersion( "4.3.0" )]
[assembly: AssemblyFileVersion( "4.3.0" )]
[assembly: AssemblyInformationalVersion( "4.3.0" )]