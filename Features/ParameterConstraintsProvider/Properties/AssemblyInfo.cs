﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle( "Tecan.Sila2.ParameterDefaultsProvider" )]
[assembly: AssemblyDescription( "Generic server-side support for runtime parameter constraints" )]

[assembly: InternalsVisibleTo( "Tecan.Sila2.ParameterDefaultsProvider.Tests" )]