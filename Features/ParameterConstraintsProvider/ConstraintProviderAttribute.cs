﻿using System;

namespace Tecan.Sila2.ParameterConstraintsProvider
{
    /// <summary>
    /// Denotes an attribute that adds a runtime parameter constraint
    /// </summary>
    [Obsolete("This class is obsolete and will be removed in future versions. There is no longer a need to use it")]
    public abstract class ConstraintProviderAttribute : Attribute
    {
    }
}
