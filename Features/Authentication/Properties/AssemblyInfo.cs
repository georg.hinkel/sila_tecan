﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Tecan.Sila2.Auth")]
[assembly: AssemblyDescription("Serverside Authentication/Authorization support")]

[assembly:InternalsVisibleTo( "Tecan.Sila2.Authentication.Tests" )]