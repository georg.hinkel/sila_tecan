﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Tecan.Sila2.Discovery;
using Tecan.Sila2.Server.Binary;
using Tecan.Sila2.Server.ServerPooling;
using Tecan.Sila2.Server.ServiceDefinition;
using Tecan.Sila2.ServerPooling;

namespace Tecan.Sila2.Server
{
    /// <summary>
    /// Base class for all SiLA 2 Servers.
    /// </summary>
    [Export( typeof( ISiLAServer ) )]
    [Export( typeof( ISiLAService ) )]
    [PartCreationPolicy( CreationPolicy.Shared )]
    public partial class SiLAServer : ISiLAService, ISiLAServer
    {

        #region Members

        private readonly ILog _logger = LogManager.GetLogger<SiLAServer>();

        /// <summary>The list of implmented features.</summary>
        private readonly List<IFeatureProvider> _implementedFeatures;

        /// <summary>Discovery listener</summary>
        private readonly ServiceAnnouncer _announcer;

        /// <summary>All current observable command executions, with the identifying CommandExecutionUUID, the command identifier and the current execution state.</summary>
        private readonly List<ObservableCommandExecution> _commandExecutions = new List<ObservableCommandExecution>();

        private readonly IFileManagement _fileManagement;
        private readonly IConfigurationStore _configurationStore;
        private readonly BinaryUpload _upload;
        private readonly BinaryDownload _download;
        private readonly LoggingInterceptor _loggingInterceptor;

        private readonly Timer _cleanupTimer;

        private readonly ServerPoolDispatcher _poolDispatcher;
        private readonly IServerPoolConnectionFactory _poolConnectionFactory;
        private readonly RequestPipeline _requestPipeline;
        private readonly string _configurationName;

        #endregion

        #region Utils

        /// <summary>
        /// Check if the specified TCP port is available
        /// If the port is already in use, an SocketException is thrown
        /// </summary>
        private static void EnsurePortIsAvailable( int port )
        {
            TcpListener tcpListener = new TcpListener( IPAddress.Parse( "0.0.0.0" ), port );
            tcpListener.Start();
            tcpListener.Stop();
        }

        #endregion

        #region Properties

        /// <inheritdoc />
        public string ServerName { get; private set; }

        /// <inheritdoc />
        public string ServerDescription { get; }

        /// <inheritdoc />
        public string ServerVendorURL { get; }

        /// <inheritdoc />
        public string ServerVersion { get; }

        /// <inheritdoc />
        public string ServerType { get; }

        /// <inheritdoc />
        public string ServerUUID { get; }

        internal RequestPipeline Pipeline => _requestPipeline;

        /// <inheritdoc />
        public void SetServerName( string serverName )
        {
            if(serverName.Length > 255)
            {
                throw new ArgumentException( "The server name must not be longer than 255 characters", nameof( serverName ) );
            }
            ServerName = serverName;
            var configuration = _configurationStore.Read<ServerConfiguration>( _configurationName );
            if(configuration != null)
            {
                configuration.Name = serverName;
                try
                {
                    _configurationStore.Write( _configurationName, configuration );
                }
                catch(Exception exception)
                {
                    _logger.Error( "Could not save server configuration", exception );
                }
            }
        }

        #endregion

        #region Constructors and destructors

        private void TryPersistConfiguration( ServerStartInformation startupInfo, IConfigurationStore configurationStore, int port )
        {
            try
            {
                configurationStore.Write( _configurationName, new ServerConfiguration
                {
                    Identifier = ServerUUID,
                    InterfaceFilter = startupInfo.NetworkInterfaces,
                    Name = startupInfo.Name,
                    Port = port
                } );
            }
            catch(Exception exception)
            {
                _logger.Warn( $"Could not save server configuration: {exception.Message}" );
            }
        }

        private void Cleanup( object state )
        {
            lock(_commandExecutions)
            {
                for(int i = _commandExecutions.Count - 1; i >= 0; i--)
                {
                    var commandExecution = _commandExecutions[i];
                    var responseTask = commandExecution.Command?.Response;
                    if(responseTask != null && (responseTask.IsCanceled || responseTask.IsCompleted || responseTask.IsFaulted))
                    {
                        if(!commandExecution.Expiration.HasValue)
                        {
                            commandExecution.Expiration = DateTime.Now + TimeSpan.FromMinutes( 5 );
                        }
                        else if(commandExecution.Expiration.Value < DateTime.Now)
                        {
                            _logger.Debug( $"Forgetting command execution {commandExecution.CommandExecutionID} ({commandExecution.CommandIdentifier})." );
                            if(commandExecution.Command is IDisposable disposable)
                            {
                                disposable.Dispose();
                            }
                            _commandExecutions.RemoveAt( i );
                        }
                    }
                }
            }
        }

        #endregion

        #region gRPC server

        /// <summary>
        /// Starts the server
        /// </summary>
        public void StartServer()
        {
            _logger.Info( "Starting server" );
            if(_announcer != null)
            {
                _announcer.Start();
                _logger.Info( $"Announcing as '{_announcer.Profile.FullyQualifiedName}'" );
            }
        }

        /// <summary>
        /// Shuts down the server
        /// </summary>
        /// <returns>An awaitable task that shuts down the server</returns>
        public Task ShutdownServer()
        {
            return Task.Factory.StartNew( () =>
            {
                lock(_commandExecutions)
                {
                    foreach(var execution in _commandExecutions)
                    {
                        if(execution.Command.IsCancellationSupported)
                        {
                            execution.Command.Cancel();
                        }
                    }
                }
                _announcer?.Dispose();
                _cleanupTimer.Dispose();
            } );
        }

        #endregion

        #region Feature discovery

        /// <summary>
        /// Gets the feature object with the given feature identifier.
        /// </summary>
        /// <param name="featureIdentifier">The (full qualified) feature identifier.</param>
        /// <returns>The feature description.</returns>
        public string GetFeatureDefinition( string featureIdentifier )
        {
            IdentifierConstraint.CheckParameter( featureIdentifier, IdentifierType.FeatureIdentifier, nameof( featureIdentifier ) );
            if(!ImplementedFeatures.Contains( featureIdentifier ))
            {
                throw new UnimplementedFeatureException( $"Given Feature identifier: \"{featureIdentifier}\"" );
            }

            var feature = _implementedFeatures.First( f => f.FeatureDefinition.FullyQualifiedIdentifier == featureIdentifier ).FeatureDefinition;
            return FeatureSerializer.SaveToString( feature ).Replace( Environment.NewLine, string.Empty );
        }

        /// <summary>
        /// Gets a list of fully qualified identifiers of the implemented features.
        /// </summary>
        public ICollection<string> ImplementedFeatures
        {
            get { return _implementedFeatures.Select( feature => feature.FeatureDefinition.FullyQualifiedIdentifier ).ToList(); }
        }

        /// <summary>
        /// Gets the features implemented by the current server
        /// </summary>
        public IEnumerable<IFeatureProvider> Features => _implementedFeatures.AsReadOnly();

        /// <inheritdoc />
        public IServerErrorHandling ErrorHandling { get; } = ServerErrorHandling.Instance;

        /// <inheritdoc />
        public ObservableCommandExecution[] FindCommandExecutions( Predicate<CommandState> stateFilter )
        {
            if(stateFilter == null)
            {
                throw new ArgumentNullException( nameof( stateFilter ) );
            }

            lock(_commandExecutions)
            {
                return _commandExecutions
                    .Where( ex => stateFilter( ex.Command.State.State ) )
                    .ToArray();
            }
        }

        #endregion

        #region Observable Commands

        /// <summary>
        /// Invokes the given (non-observable) command including the complete request pipeline
        /// </summary>
        /// <typeparam name="TRequest">The type of the request</typeparam>
        /// <typeparam name="TResponse">The type of the response</typeparam>
        /// <param name="command">The actual command</param>
        /// <param name="request">The request object</param>
        /// <param name="metadataRepository">The metadata entries to consider for the command</param>
        /// <returns>An awaitable task with the command response</returns>
        public Task<TResponse> InvokeCommand<TRequest, TResponse>( Func<TRequest, TResponse> command, TRequest request,
            IMetadataRepository metadataRepository )
            where TRequest : ISilaRequestObject
        {
            return Task.Factory.StartNew( () => InvokeCommandCore( command, request, metadataRepository ) );
        }

        private TResponse InvokeCommandCore<TRequest, TResponse>( Func<TRequest, TResponse> command, TRequest request,
            IMetadataRepository metadataRepository )
            where TRequest : ISilaRequestObject
        {
            _logger.Info( $"Executing command {request.CommandIdentifier}" );
            var interceptions = _requestPipeline.RequestInterceptions( request.CommandIdentifier, metadataRepository );
            try
            {
                var result = command( request );
                _requestPipeline.ReportSuccess( interceptions );

                return result;
            }
            catch(Exception exception)
            {
                _logger.Error( $"Error while executing command {request.CommandIdentifier}", exception );
                _requestPipeline.ReportError( interceptions, exception );

                if(exception is OperationCanceledException)
                {
                    throw ErrorHandling.CreateCancellationError( exception.Message );
                }
                else if(exception is CommandExecutionRejectedException)
                {
                    throw ServerErrorHandling.CreateFrameworkError( FrameworkErrorType.CommandExecutionNotAccepted );
                }
                else if(!(exception is RpcException))
                {
                    throw ErrorHandling.CreateUndefinedExecutionError( exception );
                }

                throw;
            }
        }

        /// <summary>
        /// Invokes the given (non-observable) command including the complete request pipeline
        /// </summary>
        /// <typeparam name="TResponse">The type of the response</typeparam>
        /// <param name="command">The actual command</param>
        /// <param name="commandIdentifier">The identifier of the command</param>
        /// <param name="metadataRepository">The metadata entries to consider for the command</param>
        /// <returns>An awaitable task with the command response</returns>
        public async Task<TResponse> InvokeCommand<TResponse>( Func<Task<TResponse>> command, string commandIdentifier, IMetadataRepository metadataRepository )
        {
            var interceptions = _requestPipeline.RequestInterceptions( commandIdentifier, metadataRepository );
            _logger.Info( $"Executing command {commandIdentifier}" );
            try
            {
                var result = await command();
                _requestPipeline.ReportSuccess( interceptions );

                return result;
            }
            catch(Exception exception)
            {
                _logger.Error( $"Error while executing command {commandIdentifier}", exception );
                _requestPipeline.ReportError( interceptions, exception );

                if(exception is OperationCanceledException)
                {
                    throw ErrorHandling.CreateCancellationError( exception.Message );
                }
                else if(exception is CommandExecutionRejectedException)
                {
                    throw ServerErrorHandling.CreateFrameworkError( FrameworkErrorType.CommandExecutionNotAccepted );
                }
                else if(!(exception is RpcException))
                {
                    throw ErrorHandling.CreateUndefinedExecutionError( exception );
                }

                throw;
            }
        }

        internal void RegisterBinaryParameter( string commandParameter )
        {
            _upload.RegisterCommandParameter( commandParameter );
        }

        /// <summary>
        /// Registers an observable command execution
        /// </summary>
        /// <param name="commandIdentifier">The unique identifier of the command that shall be executed</param>
        /// <param name="command">The observable command</param>
        /// <param name="metadataRepository">The metadata entries to consider for the command</param>
        /// <returns></returns>
        public string AddObservableCommandExecution( string commandIdentifier, IObservableCommand command, IMetadataRepository metadataRepository )
        {
            var interceptions = _requestPipeline.RequestInterceptions( commandIdentifier, metadataRepository );
            // create command ID
            var commandExecutionId = Guid.NewGuid().ToString();

            var execution = new ObservableCommandExecution( commandExecutionId, commandIdentifier, command, interceptions );

            lock(_commandExecutions)
            {
                _commandExecutions.Add( execution );
            }
            if(command is IServerCommand serverCommand)
            {
                serverCommand.CommandExecutionUuid = commandExecutionId;
            }
            if(!command.IsStarted)
            {
                command.Start();
            }

            command.Response.ContinueWith( execution.Complete );

            return commandExecutionId;
        }

        internal T GetPropertyValue<T>( string propertyIdentifier, Func<T> value, IMetadataRepository metadataRepository )
        {
            var interceptors = _requestPipeline.RequestInterceptions( propertyIdentifier, metadataRepository );
            try
            {
                _logger.Info( $"Obtaining value for property {propertyIdentifier}" );
                var result = value();
                _requestPipeline.ReportSuccess( interceptors );

                return result;
            }
            catch(Exception exception)
            {
                _logger.Error( $"Exception happened obtaining the value for {propertyIdentifier}", exception );
                _requestPipeline.ReportError( interceptors, exception );
                if(exception is RpcException)
                {
                    throw;
                }
                throw ErrorHandling.CreateUndefinedExecutionError( exception );
            }
        }

        /// <inheritdoc />
        public Stream ResolveStoredBinary( string identifier )
        {
            if(!_upload.IsUploadCompleted( identifier ))
            {
                throw new ArgumentException( $"Upload of the binary {identifier} is not yet complete" );
            }
            return _fileManagement.OpenRead( identifier );
        }

        /// <inheritdoc />
        public bool ShouldStoreBinary( long length )
        {
            return length > 200 * 1024;
        }

        /// <inheritdoc />
        public string StoreBinary( Stream data )
        {
            var identifier = _fileManagement.CreateNew( data.Length );
            using(var stream = _fileManagement.OpenWrite( identifier ))
            {
                data.CopyTo( stream );
            }

            return identifier;
        }

        /// <inheritdoc />
        public string StoreBinary( FileInfo file )
        {
            return _fileManagement.CreateIdentifier( file );
        }

        /// <inheritdoc />
        public void Delete( string identifier )
        {
            _upload.DeleteUpload( identifier );
        }

        /// <inheritdoc />
        public FileInfo ResolveStoredBinaryFile( string identifier )
        {
            if(!_upload.IsUploadCompleted( identifier ))
            {
                throw new ArgumentException( $"Upload of the binary {identifier} is not yet complete" );
            }
            return _fileManagement.Locate( identifier );
        }

        /// <inheritdoc />
        public ObservableCommandExecution GetCommandExecution( string commandExecutionId )
        {
            lock(_commandExecutions)
            {
                var command = _commandExecutions.Find( c => c.CommandExecutionID == commandExecutionId );
                if(command == null)
                {
                    // if command execution ID hasn't been found -> raise SiLA Framework Error
                    throw ServerErrorHandling.CreateFrameworkError( FrameworkErrorType.InvalidCommandExecutionUuid );
                }

                return command;
            }
        }

        /// <inheritdoc />
        public IDisposable ConnectToPool( string poolAddress )
        {
            var connection = new PoolConnection( _poolDispatcher, _poolConnectionFactory.CreatePoolConnection( poolAddress ).AsyncDuplexStreamingCall( PoolingConstants.ConnectSilaServerMethod, null, new CallOptions() ) );
            connection.Start();
            return connection;
        }

        #endregion
    }
}
